// import fs from 'fs'
// import path from 'path'

// env vars?
// yaml?
//
// user defined partial configs?
//
// @Configuration('someKey') // @singleton
// class SomeConfig {
//   key1: typeof key1
//   key2: typeof key2
// }
export class Config {
  port = 8080
  address = '0.0.0.0'

  // constructor(path: string) {
  constructor() {
    // const userConfig = this.readConfig(path)
    // this.port = userConfig.port ?? this.port
    // this.address = userConfig.address ?? this.address
    console.log('config initialized')
  }

  // private readConfig(projectPath: string): Partial<Config> {
  //   const configPath = projectPath + '/resources/application.json'
  //   const resolvedConfigPath = path.resolve(configPath)
  //   const buf = fs.readFileSync(resolvedConfigPath)
  //   const str = buf.toString()
  //   const json = JSON.parse(str)
  //   return json
  // }
}

// export function fromEnv<T>(
//   key: string,
//   fallback: T,
//   parse: (a: string) => T = (a) => (a as unknown) as T,
// ): T {
//   const value = process.env[key]
//   if (value === undefined) return fallback
//   return parse(value)
// }
