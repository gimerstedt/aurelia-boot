import { singleton } from '@aurelia/kernel'
import { EndpointRegistration } from './endpoint-registration'
import { TsBoot } from './ts-boot'
import { Method } from './types'

export const ServiceDecorator = singleton

export function ControllerDecorator(controllerPath: string) {
  return function (controller: Function) {
    TsBoot.container
      .get(EndpointRegistration)
      .finalizeController(controllerPath, controller)
  }
}

function methodDeco(method: Method) {
  return function (path: string) {
    return function (_target: any, key: string) {
      TsBoot.container
        .get(EndpointRegistration)
        .registerMethod({ path, fn: key, method })
    }
  }
}

export const GetDecorator = methodDeco('get')
export const PostDecorator = methodDeco('post')
export const DeleteDecorator = methodDeco('delete')
export const PutDecorator = methodDeco('put')
