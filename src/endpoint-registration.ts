import { singleton } from '@aurelia/kernel'
import { Endpoint } from './types'

@singleton
export class EndpointRegistration {
  private currentController: Endpoint[] = []
  private endpoints: Endpoint[] = []

  registerMethod(e: Endpoint) {
    this.currentController.push(e)
  }

  finalizeController(controllerPath: string, controller: Function) {
    for (const e of this.currentController) {
      const path = (controllerPath + e.path).replace('//', '/')
      this.endpoints.push({ ...e, path, controller })
    }
    this.currentController.length = 0
  }

  getAll() {
    return this.endpoints
  }
}
