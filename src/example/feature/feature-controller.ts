import { Controller, Get } from '../..'
import { FeatureService } from './feature-service'

@Controller('/feature')
export class FeatureController {
  // optional :/
  log?: typeof console

  constructor(private service: FeatureService) {}

  @Get('/get-all')
  getAllFeatures() {
    this.log?.info('get all features in controller')
    return this.service.getFeatures()
  }
}

export default FeatureController
