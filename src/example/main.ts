import TsBoot from '../ts-boot'
// import * as sources from './sources'
import { sources } from './sources'

// TsBoot.fromHere().run()
// TsBoot.sources(import('./sources')).run()
// TsBoot.sources(sources).run()
TsBoot.sources(sources).run()
