import { DI } from '@aurelia/kernel'
import { RootController } from './root-controller'

// use ts-mockito?
describe('RootController', () => {
  const c = DI.createContainer()
  it('works?', () => {
    const sut = c.get(RootController)

    const r = sut.root()
    expect(r).toEqual([])
  })
})
