import { Controller, Delete, Get, Post } from '..'

@Controller('/')
export class RootController {
  private things: any[] = []

  @Get('/')
  root() {
    return this.things
  }

  @Post('/')
  addThing(thing: any) {
    this.things.push(thing)
  }

  @Delete('/') // :id params?
  removeAllThings() {
    this.things.length = 0
  }
}

export default RootController
