import {
  ControllerDecorator,
  DeleteDecorator,
  GetDecorator,
  PostDecorator,
  PutDecorator,
  ServiceDecorator,
} from './decorators'
import TsBoot from './ts-boot'

export const Get = GetDecorator
export const Post = PostDecorator
export const Delete = DeleteDecorator
export const Put = PutDecorator
// people use Patch?

// WebSocket?

export const Controller = ControllerDecorator

// transient/request-scope ???
export const Service = ServiceDecorator

export default TsBoot
