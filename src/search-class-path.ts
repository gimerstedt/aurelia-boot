import { resolve } from 'path'
import { recFindByExt } from './util'

export async function searchClassPath<T>(projectRoot: string): Promise<T[]> {
  const resolvedPath = resolve(projectRoot)
  const controllerPaths = recFindByExt(resolvedPath)
  return Promise.all(controllerPaths.map((p) => import(p)))
}
