import { Key, singleton } from '@aurelia/kernel'
import fastify from 'fastify'
import { Config } from './config'
import { EndpointRegistration } from './endpoint-registration'
import { TsBoot } from './ts-boot'

@singleton
export class Server {
  // make server agnostic?
  // express?
  // koa?
  server = fastify({ logger: true })

  constructor(
    private config: Config,
    private endpoints: EndpointRegistration,
  ) {}

  mountAll() {
    for (const { controller, method, path, fn } of this.endpoints.getAll()) {
      console.info('mounting:', method.toUpperCase(), path)
      this.server[method](path, (req, res) => {
        const controllerInstance = TsBoot.container.get(controller as Key)

        // fn(req, res) ???
        // params/query?
        // response from controller?
        // catch?
        const response = controllerInstance[fn](req.body)
        res.send(response)
      })
    }
  }

  start() {
    this.mountAll()
    return this.server.listen(this.config.port, this.config.address)
  }
}
