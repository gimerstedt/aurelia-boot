import { DI, Registration } from '@aurelia/kernel'
import { dirname } from 'path'
import { Config } from './config'
import { searchClassPath } from './search-class-path'
import { Server } from './server'
import { Controller, GetControllers, GetSources, Sources } from './types'

export class TsBoot {
  static container = DI.createContainer()

  constructor(private getControllers: GetControllers) {}

  /**
   * example usage:
   * TsBoot.sources(import('./sources')).run()
   *
   * import * as sources from './sources' // { [key:string] : Controller }
   * TsBoot.sources(sources).run()
   *
   * import { sources } from './sources' // Controller[]
   * TsBoot.sources(sources).run()
   */
  static sources(arg: Controller[] | Sources | GetSources | any) {
    const getControllers = async () => {
      if (Array.isArray(arg)) return arg
      const s = typeof arg === 'function' ? await arg() : arg
      return Object.entries<Controller>(s).map((a) => a[1])
    }
    return new TsBoot(getControllers)
  }

  static fromHere() {
    const parentFile = module?.parent?.filename
    if (!parentFile) throw Error('could not identify project root')
    const path = dirname(parentFile)
    return new TsBoot(() => searchClassPath<Controller>(path))
  }

  withConfig() {
    // use path via fromHere?
    // force config if registering sources manually?
  }

  async run() {
    try {
      const config = new Config()
      TsBoot.container.register(Registration.instance(Config, config))

      const controllers = await this.getControllers()
      TsBoot.container.register(controllers)

      TsBoot.container.get(Server).start()
    } catch (e) {
      console.error(e)
      process.exit(1)
    }
  }
}

export default TsBoot
