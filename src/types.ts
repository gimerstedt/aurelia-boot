import fastify from 'fastify'

export type GetControllers = () => Controller[] | Promise<Controller[]>
export type Controller = { new: (...args: any[]) => any }

export type GetSources = () => Sources | Promise<Sources>
export type Sources = { [key: string]: Controller }

export type Config = {
  port?: number
  address?: string
}

export type Method = 'get' | 'put' | 'post' | 'delete'

export type Endpoint = {
  controller?: Function
  path: string
  fn: string
  method: Method
}

export type Server = fastify.FastifyInstance<
  import('http').Server,
  import('http').IncomingMessage,
  import('http').ServerResponse
>
