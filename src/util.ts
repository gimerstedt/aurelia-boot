import fs from 'fs'
import path from 'path'

export function recFindByExt(
  base: string,
  files: string[] = fs.readdirSync(base),
  result: string[] = [],
  matcher = isController,
) {
  files.forEach((file) => {
    const newbase = path.join(base, file)
    if (fs.statSync(newbase).isDirectory())
      result = recFindByExt(newbase, fs.readdirSync(newbase), result)
    else if (matcher(file)) result.push(newbase)
  })

  return result
}

export function isController(fileName: string) {
  return (
    fileName.endsWith('-controller.ts') || fileName.endsWith('-controller.js')
  )
}
